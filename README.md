# Cantaloupe

#### Cantaloupe Docker repository with sample docker-compose and sample property file


!!![] This image requires a licensed version of Kakadu. Add the source code to the Kakadu-Directory


This Docker-Image has been created to compile the licensed Kakadu-Source files which should be copied into the kakadou-directory before.
Version number may be passed as ARG argument on build:

```
build:
    context: ./
    dockerfile: Dockerfile
    args:
        KAKDU_VERSION: v8_4-02174L
```

!!![] First  build may require some time to finish



## Podman

This image was realised to test Cantaloupe's access to images stored on a nexus/posix mount on our Openstack instances on MPCDL.
Logging and caching can be configured to write to the host's file system (e.g. nexus fs or dedicated shared volumes) instead of writing to the container space. Logs can grow considerably, so care should be taken to clean up the logs regularly, for example using logrotate or Cantaloupe's RollingFileAppender.  

To access files on a mounted nexus/posix-filesystem by podman in rootless mode some further configuration steps on the host system are required.

- add additional uid's and gid's for your user to the hosts subuid and subgid files:

```
/etc/subuid:
your_user_name:200000:65536

/etc/subgid:
your_user_name:200000:65536
```

- reload podman
```
podman system migrate
```

- prevent user processes to be killed once the user session completed
```
loginctl enable-linger $UID
```

- build the image with Podman:
```
podman build --tag drummerroma/cantaloupe  –-build-arg=KAKADU_VERSION=v8_4-02174L -f ./Dockerfile . 
```


- run the image:

```
podman run -d --name=cantaloupe-podman -p 8182:8182 -v /nexus/posix/YOUR-Mount:/mnt -v /nexus/posix/YOUR-Mount/logs:/var/log/cantaloupe -v /nexus/posix/YOUR-Mount/cache:/var/cache/cantaloupe -v $PWD/cantaloupe.properties:/etc/cantaloupe.properties localhost/drummerroma/cantaloupe -d
```

Port 8182 is exposed.


- enter container

```
podman exec -ti cantaloupe-podman /bin/bash
```

- view the logs

```
podman logs -f cantaloupe-podman
```



Cantaloupe must run as root user in the container. Otherwise no access to the mounted nexus filesystem will be possible.



## Example:

#### Cantloupe Server
http://podman.biblhertz.it


#### sample links

- http://podman.biblhertz.it/iiif/3/%2Fdlib03%2FPE-0001%2FPE-0001-0004.jp2/info.json

- http://podman.biblhertz.it/iiif/3/%2Fdlib03%2FPE-0001%2FPE-0001-0004.jp2/full/max/0/default.jpg

- http://podman.biblhertz.it/iiif/3/%2Fdlib03%2Ffa17030201%2F0082.jp2/full/max/0/default.jpg

- http://podman.biblhertz.it/iiif/3/%2Ffotothek%2FIIIF%2FFotothek%2FPositive%2FFotoalben%2FAlbum13%2Fbhim00049510.jp2/full/max/0/default.jpg

!!![] Obervations: this is a test server with very limited resources. Cantaloupe cache is not optimised, so don't aspect quick response times for large images.

