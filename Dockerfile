# build on m2 osx
# podman buildx build --platform linux/amd64 --tag drummerroma/cantaloupe -f ./Dockerfile
# build on Linux
# podman build --tag drummerroma/cantaloupe -f ./Dockerfile
FROM openjdk:21-slim-buster
LABEL org.opencontainers.image.authors="drummerroma@gmail.com"

ARG KAKDU_VERSION="v8_4-02174L"
ENV VERSION=5.0.5
ENV LIBJPEGTURBO=2.1.2

EXPOSE 80 8182

RUN apt-get update && apt-get install -y --no-install-recommends \
  build-essential curl unzip gettext-base jq libopenjp2-tools \
  x11-apps cmake nasm \
  && rm -rf /var/lib/apt/lists/*


# build kakadu
COPY kakadu/"${KAKDU_VERSION}" /build/kakadu/"${KAKDU_VERSION}"

RUN if [ ! -z "${KAKDU_VERSION}" ]; then \
      cd /build/kakadu/${KAKDU_VERSION}/make && \
      make -f Makefile-Linux-x86-64-gcc && \
      mkdir /build/kakadu/lib && \
      mkdir /build/kakadu/bin && \
      cp ../lib/Linux-x86-64-gcc/*.so /build/kakadu/lib && \
      cp ../bin/Linux-x86-64-gcc/kdu_compress /build/kakadu/bin && \
      cp ../bin/Linux-x86-64-gcc/kdu_expand /build/kakadu/bin && \
      cp ../bin/Linux-x86-64-gcc/kdu_jp2info /build/kakadu/bin ; \
    else \
      mkdir -p /build/kakadu/lib && \
      mkdir -p /build/kakadu/bin && \
      touch /build/kakadu/lib/placeholder.so && \
      touch /build/kakadu/bin/kdu_placeholder ; \
    fi

RUN mkdir /usr/lib/jni
WORKDIR /usr/lib/jni
RUN cp /build/kakadu/lib/* /usr/lib/jni/

WORKDIR /usr/local/bin
RUN cp /build/kakadu/bin/* /usr/local/bin/

ENV CANTALOUPE_PROPERTIES="/etc/cantaloupe.properties"

# Install libjpeg-turbo
RUN curl -L "https://github.com/libjpeg-turbo/libjpeg-turbo/archive/refs/tags/$LIBJPEGTURBO.zip" > /tmp/libjpegturbo.zip \
  && unzip /tmp/libjpegturbo.zip -d /tmp \
  && cd /tmp/libjpeg-turbo-$LIBJPEGTURBO \
  && cmake -G"Unix Makefiles" -DWITH_JAVA=1 \
  && make \
  && mkdir -p /opt/libjpeg-turbo/lib/ \
  && cp libturbojpeg.so /opt/libjpeg-turbo/lib/libturbojpeg.so

# Retrieve Cantaloupe executable
RUN curl -L "https://github.com/cantaloupe-project/cantaloupe/releases/download/v$VERSION/cantaloupe-$VERSION.zip" > /tmp/cantaloupe.zip \
  && mkdir -p /usr/local/ \
  && cd /usr/local \
  && unzip /tmp/cantaloupe.zip -d /usr/local \
  && ln -s cantaloupe-$VERSION cantaloupe \
  && rm /tmp/cantaloupe.zip \
  && cp /usr/local/cantaloupe-$VERSION/deps/Linux-x86-64/lib/* /lib

RUN touch /etc/cantaloupe.properties

RUN mkdir -p /var/log/cantaloupe \
 && mkdir -p /var/cache/cantaloupe 

RUN mkdir -p /var/log/cantaloupe /var/cache/cantaloupe 

CMD java -Dcantaloupe.config=$CANTALOUPE_PROPERTIES -Xmx2g -jar /usr/local/cantaloupe/cantaloupe-$VERSION.jar